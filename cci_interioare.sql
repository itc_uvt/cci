-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2018 at 01:14 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cci_interioare`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts_dynamic`
--

CREATE TABLE `contacts_dynamic` (
  `cnp` varchar(13) NOT NULL,
  `titlu` varchar(255) NOT NULL,
  `nume` varchar(255) NOT NULL,
  `prenume` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts_dynamic`
--

INSERT INTO `contacts_dynamic` (`cnp`, `titlu`, `nume`, `prenume`) VALUES
('1920819011169', 'Prof', 'Ionescu', 'Florin'),
('1930819011169', 'Traducator', 'Popescu', 'Florina'),
('1940819011169', 'Lect', 'Georgescu', 'Bogdan'),
('1950819011169', 'Lect', 'Marinescu', 'Robert'),
('1980819011169', 'Programator', 'Solyom', 'Felix Stefan');

-- --------------------------------------------------------

--
-- Table structure for table `contacts_static`
--

CREATE TABLE `contacts_static` (
  `cnp` varchar(13) NOT NULL,
  `departament` varchar(255) NOT NULL,
  `prescurtare` varchar(100) NOT NULL,
  `domeniu` varchar(255) NOT NULL,
  `camera` varchar(5) NOT NULL,
  `interior` varchar(5) NOT NULL,
  `fax` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts_static`
--

INSERT INTO `contacts_static` (`cnp`, `departament`, `prescurtare`, `domeniu`, `camera`, `interior`, `fax`, `email`) VALUES
('1920819011169', 'Matematica', 'M', 'Matematica', '151', '111', '789789789', 'ionescu.florin@e-uvt.ro'),
('1930819011169', 'Romana-Engleza', 'RE', 'Filologie', '120', '222', '567567567', 'popescu@e-uvt.ro'),
('1940819011169', 'Cinematografie', 'CF', 'Cinematografie', '153', '333', '345345345', 'georgescu@e-uvt.ro'),
('1950819011169', 'Franceza', 'FR', 'Filologie', '154', '444', '123123123', 'marinescu@e-uvt.ro'),
('1980819011169', 'Informatica', 'ITC', 'Informatica', '150', '', '', 'felix.solyom98@e-uvt.ro');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_name`, `user_password`) VALUES
(1, 'felix.solyom98@e-uvt.ro', 'Stefan Solyom', '$2y$10$NKObGkt0l7XD8EnSN/NAT.l3zrEilU1wIFGJb.gbw0p3pxPUnQyBe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts_dynamic`
--
ALTER TABLE `contacts_dynamic`
  ADD PRIMARY KEY (`cnp`);

--
-- Indexes for table `contacts_static`
--
ALTER TABLE `contacts_static`
  ADD PRIMARY KEY (`cnp`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
