<?php
require_once 'config/connection.php';
require_once 'config/session.php';

$page = isset($_GET['page']) 
   ? preg_replace("/\W+/", "", $_GET['page'])
   : "home";
   
switch($page) {
    case "home" : $title = "Vizualizare"; break;
    case "upload" : $title = "Incarcare"; break;
    case "users" : $title = "Utilizatori"; break;
    case "password" : $title = "Schimbare Parola"; break;
    default: $title = "404 | Pagina nu a putut fi gasita!"; break;
}

include_once 'partials/html.header.php';
echo '<div class="wrapper">';
include_once 'partials/html.sidebar.php';
echo '<div class="main-panel">';
include_once 'partials/html.navbar.php';
echo '<div class="content">';
echo '<div class="container-fluid">';
if(file_exists("pages/$page.php")) { include_once "pages/$page.php"; }
else { include_once "pages/404.php"; }
echo '</div>';
echo '</div>';
echo '</div>';
echo '</div>';
include_once 'partials/html.footer.php';
?>