function sendData(filename, columns, table){
    $.ajax({
        type: "POST",
        url: "import.php",
        data: {
            file: filename,
            columns: columns,
            table: table
        },
        async: true,
        success: _=> window.location.href = '../admin.php'
    })
}