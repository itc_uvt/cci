let http;
if(window.XMLHttpRequest) {
    http = new XMLHttpRequest()
} else if(window.ActiveXObject) {
    http = new ActiveXObject("Microsoft.XMLHttp")
}

function doSearch(query) {
    if(http) {
        http.open("GET", query)
        http.send(null)
        http.onreadystatechange = function() {
            if(http.readyState == 4 && http.status == 200) {
                let results = document.getElementById("results")
                results.innerHTML = http.responseText
            }
        }
    }
}

function search(event) {
    event = event ? event : window.event
    input = event.target ? event.target : event.srcElement
    if(event.type === "keyup") {
        let results = document.getElementById("results");
        results.innerHTML = "";
        if(input.value) {
            doSearch(`config/search.php?term=${input.value}`)
        }
    }
}