<?php
require_once 'config/connection.php';
require_once 'config/variables.php';

session_start();
if(isset($_SESSION['email']) || !empty($_SESSION['email'])){
    header("location: admin.php");
    exit;
}

$email = $password = $emailErr = $passwordErr = "";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty(trim($_POST["email"]))) {
        $emailErr = EMAIL_EMPTY;
    } else {
        $email = trim($_POST["email"]);
    }

    if(empty(trim($_POST["password"]))) {
        $passwordErr = PASSWORD_EMPTY;
    } else {
        $password = trim($_POST["password"]);
    }

    if(empty($emailErr) && empty($passwordErr)) {
        $sql = "SELECT user_email, user_password FROM users WHERE user_email = ?";
        
        if($stmt = $mysqli->prepare($sql)) {

            $param_user_email = $email;
            $stmt->bind_param("s", $param_user_email);

            if($stmt->execute()) {
                $stmt->store_result();

                if($stmt->num_rows == 1) {
                    $stmt->bind_result($email, $hashed_password);
                    if($stmt->fetch()) {
                        if(password_verify($password, $hashed_password)) {
                            $_SESSION["email"] = $email;
                            header("location: admin.php");
                        } else {
                            $passwordErr = PASSWORD_FAIL;
                        }
                    }
                } else {
                    $emailErr = EMAIL_FAIL;
                }
            } else {
                $emailErr = ERROR;
            }
        }
        $stmt->close();
    }
    $mysqli->close();
}
?>
<!DOCTYPE html> 
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Logare CCI Interne | UVT</title>
    
    <link href="stylesheets/login.css" rel="stylesheet">
    <link href="stylesheets/bootstrap.min.css" rel="stylesheet">
    <link href="stylesheets/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<div class="container">
    <form class="login" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
    <img class="center-block logo" src="images/logo_uvt_centenar.png" alt="">
    
    <hr />
 
    <?php if(!empty($emailErr) || !empty($passwordErr)) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $emailErr; ?> 
        <?php echo $passwordErr; ?> 
    </div>
    <?php endif; ?>

    <div class="form-group">
        <label for="email" class="sr-only">Adresa de email</label>
        <input type="email" name="email" class="form-control" placeholder="Adresa dvs. de email" required autofocus>
    </div>
    
    <div class="form-group">
        <label for="password" class="sr-only">Parola</label>
        <input type="password" name="password" class="form-control" placeholder="Parola dvs." required>
    </div>

    <div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Autentificare</button>
    </div>
    </form>
</div>

</body>
<script src='javascripts/jquery-3.1.1.min.js' type='text/javascript'></script>
<script src='javascripts/bootstrap.min.js' type='text/javascript'></script>
</html>