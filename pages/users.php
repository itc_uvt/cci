<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-7">
            <?php if(!empty($_SESSION["remove_errors"])) : ?>
            <div class="alert alert-danger" role="alert">
                <?php
                foreach($_SESSION["remove_errors"] as $error) { echo '<p>' . $error . '</p>'; }
                unset($_SESSION["remove_errors"]); 
                ?>
            </div>
            <?php endif; ?>
            <div class="card">
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nume si Prenume</th>
                                <th>Email</th>
                                <th>Actiuni</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $sql = "SELECT user_id, user_email, user_name FROM users;";
                            $result = $mysqli->query($sql);

                            if($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<tr>";
                                    echo "<td>" . $row["user_id"] . "</td>";
                                    echo "<td>" . $row["user_name"] . "</td>";
                                    echo "<td>" . $row["user_email"] . "</td>";
                                    echo '<td>
                                            <a href="admin.php?page=\'password\'&amp;user=\''.$row["user_id"].'\'" class="btn btn-xs btn-primary"><i class="fa fa-gear"></i></a>
                                            <a href="config/remove-user.php?user=\''.$row["user_id"].'\'" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i></a>
                                        </td>';
                                    echo "</tr>";
                                }
                            }
                            $result->close();
                            $mysqli->close();
                            ?>    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>   
        <div class="col-md-5">
            <div class="card">
                <div class="header">
                    <h4 class="title">Adaugati un utilizator</h4>
                </div>
                <div class="content">
                    <form method="post" action="config/add-user.php">
                        <?php if(!empty($_SESSION["register_errors"])) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?php
                            foreach($_SESSION["register_errors"] as $error) {
                                echo '<p>' . $error . '</p>';
                            }
                            unset($_SESSION["register_errors"]); 
                            ?>
                        </div>
                        <?php endif; ?>
        
                        <div class="form-group">
                            <label for="name" class="sr-only">Nume si prenume</label>
                            <input type="text" name="name" class="form-control" placeholder="Nume si prenume" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="email" class="sr-only">Adresa de email</label>
                            <input type="email" name="email" class="form-control" placeholder="Adresa de email" required>
                        </div>

                        <div class="form-group">
                            <label for="password" class="sr-only">Parola</label>
                            <input type="password" name="password" class="form-control" placeholder="Parola" required>
                        </div>

                        <div class="form-group">
                            <button class="btn  btn-primary btn-block" type="submit">Adaugare</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>     
    </div>
</div>