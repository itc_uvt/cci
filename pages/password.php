<?php 
if(isset($_GET["user"])) {
    $userId = $mysqli->real_escape_string(preg_replace("/\W+/", "", $_GET['user']));
} else {
    header("location: admin.php?page='users'");
}
?>
<div class="col-md-4 col-md-offset-4">
    <div class="row">
        <div class="card">
            <div class="content">
                <form method="post" action="config/password-user.php">
                    <?php if(!empty($_SESSION["password_errors"])) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?php                                 
                        foreach($_SESSION["password_errors"] as $error) { echo '<p>' . $error . '</p>'; }
                        unset($_SESSION["password_errors"]); 
                        ?>
                    </div>
                    <?php endif; ?>                
                    <div class="form-group">
                        <label for="newPassword" class="sr-only">Parola Noua</label>
                        <input type="password" name="newPassword" class="form-control" placeholder="Parola Noua" required>
                    </div>             
                    <div class="form-group">
                        <label for="confirmNewPassword" class="sr-only">Confirmare Parola Noua</label>
                        <input type="password" name="confirmNewPassword" class="form-control" placeholder="Confirmare Parola Noua" required>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="userId" value="<?php echo htmlspecialchars($userId); ?>">
                        <button class="btn btn-primary btn-block" type="submit">Schimbare parola</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


