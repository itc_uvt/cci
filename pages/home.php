<div class="col-md-12">
    <div class="row">
        <div class="card">
            <div class="content">
                <h5><i class="fa fa-lg fa-table text-success"></i> Descarcarea listei de contacte interne UVT se poate face dand click <a href="config/export.php"><i class="fa fa-link"></i> aici</a>.</h5>
            </div>
        </div>
    </div>
    <?php if(!empty($_SESSION["upload_error"])) : ?>
    <div class="row">        
        <div class="alert alert-danger" role="alert">
            <?php 
            echo $_SESSION["upload_error"]; 
            unset($_SESSION["upload_error"]); 
            ?> 
        </div>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="card">
            <div class="content">
                <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Titlu</th>
                            <th>Nume</th>
                            <th>Prenume</th>
                            <th>Departament</th>
                            <th>Prescurtare</th>
                            <th>Domeniu</th>
                            <th>Camera</th>
                            <th>Interior</th>
                            <th>Fax</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $sql = "SELECT * FROM contacts_dynamic LEFT JOIN contacts_static ON contacts_static.cnp = contacts_dynamic.cnp";
                        $result = $mysqli->query($sql);

                        if($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                echo "<tr>";
                                echo "<td>" . $row["titlu"] . "</td>";
                                echo "<td>" . $row["nume"] . "</td>";
                                echo "<td>" . $row["prenume"] . "</td>";
                                echo "<td>" . $row["departament"] . "</td>";
                                echo "<td>" . $row["prescurtare"] . "</td>";
                                echo "<td>" . $row["domeniu"] . "</td>";
                                echo "<td>" . $row["camera"] . "</td>";
                                echo "<td>" . $row["interior"] . "</td>";
                                echo "<td>" . $row["fax"] . "</td>";
                                echo "<td>" . $row["email"] . "</td>";
                                echo "</tr>";
                            }
                        }
                        $result->close();
                        $mysqli->close();
                        ?>    
                    </tbody>
                </table>
                </div>
                
            </div>
        </div>
    </div>
</div>              