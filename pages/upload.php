<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="header"><h4 class="title">Date Personale</h4></div>
                <div class="content">
                    <form action="config/upload.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input id="importer" name="importer" type="file" class="form-control file" data-show-preview="false">
                            <input type="hidden" name="importType" value="dynamic">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="header"><h4 class="title">Date Profesionale si Contact</h4></div>
                <div class="content">
                    <form action="config/upload.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <input id="importer" name="importer" type="file" class="form-control file" data-show-preview="false">
                            <input type="hidden" name="importType" value="static">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</div>