<?php
require_once "connection.php";
require_once "variables.php";

if($mysqli->set_charset("utf8")) {
    $searchTerm = $mysqli->real_escape_string($_GET["term"]);
    $sql = "SELECT contacts_dynamic.titlu, contacts_dynamic.nume, contacts_dynamic.prenume,
            contacts_static.departament, contacts_static.prescurtare, contacts_static.domeniu,
            contacts_static.camera, contacts_static.interior, contacts_static.fax, contacts_static.email
            FROM contacts_dynamic LEFT JOIN contacts_static ON contacts_dynamic.cnp = contacts_static.cnp
            WHERE CONCAT_WS(' ', contacts_dynamic.nume, contacts_dynamic.prenume) LIKE '$searchTerm%' 
            OR contacts_dynamic.nume LIKE '$searchTerm%' OR contacts_dynamic.prenume LIKE '$searchTerm%'
            OR contacts_dynamic.titlu LIKE '$searchTerm%' OR contacts_static.departament LIKE '$searchTerm%' 
            OR contacts_static.prescurtare LIKE '$searchTerm%' OR contacts_static.camera LIKE '$searchTerm%' 
            OR contacts_static.interior LIKE '$searchTerm%' OR contacts_static.email LIKE '$searchTerm%' 
            OR contacts_static.domeniu LIKE '$searchTerm%' OR contacts_static.fax LIKE '$searchTerm%';";

    if((!empty(trim($searchTerm))) && (strlen($searchTerm) > 2)) {
        $result = $mysqli->query($sql);
        if(!$result) { trigger_error($mysqli->error);}
        if($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                echo '<li class="list-group-item d-flex justify-content-between align-items-center">
                <h4 class="text-primary">' . $row["titlu"] . ' ' . $row["prenume"] . ' <span class="text-uppercase">' . $row["nume"] . '</span></h4>
                <h5>Email: ' . $row["email"] . '</h5>
                <h5>' . $row["departament"] . ' - ' . $row["domeniu"] . '</h5>
                <h5>Cabinet: ' . $row["camera"] . ' Interior: ' . $row["interior"] . ' Fax: ' . $row["fax"] . '</h5>
                </li>';
            }
        } else {
            echo '<li class="list-group-item d-flex justify-content-between align-items-center">
            <h4 class="text-primary">'.NO_RESULT.'</h4>
            </li>';
        }

    }
}
?>