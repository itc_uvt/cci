<?php 
// Error Messages
define('NAME_EMPTY', "Va rugam sa introduceti Numele si Prenumele!");
define('EMAIL_EMPTY', "Va rugam sa introduceti o adresa de email!");
define('EMAIL_TAKEN', "Acest email este  deja folosit!");
define('EMAIL_FAIL', "Adresa de email este incorecta!");
define('PASSWORD_EMPTY', "Va rugam sa introduceti parola!");
define('PASSWORD_FAIL', "Parola este incorecta!");
define('PASSWORD_CONFIRM', "Confirmati parola!");
define('PASSWORD_MISMATCH', "Parolele nu coincid!");
define('USER_MYSELF', "Nu va puteti sterge propriul cont!");
define('NO_RESULT', "Nici un rezultat!");
define('ERROR', "A intervenit o eroare!");
define('EXTENSION_MISMATCH', "Extensia fisierului trebuie sa fie: 'csv'!");

?>