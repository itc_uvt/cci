<?php 
require_once 'connection.php';
require_once 'session.php';

$csv = "export_".date('Y-m-d').".csv";

$sql = "SELECT contacts_dynamic.cnp, contacts_dynamic.titlu, contacts_dynamic.nume, contacts_dynamic.prenume,
               contacts_static.departament, contacts_static.prescurtare, contacts_static.domeniu, 
               contacts_static.camera, contacts_static.interior, contacts_static.fax, contacts_static.email
        FROM contacts_dynamic LEFT JOIN contacts_static ON contacts_static.cnp = contacts_dynamic.cnp";

$export = "";

$result = $mysqli->query($sql);
$fieldCount = $result->field_count;
$fieldNames = $result->fetch_fields();

for($i=0; $i < $fieldCount; $i++) {
    $export .= $fieldNames[$i]->name.',';
}

$export .="\r\n";

while($row = $result->fetch_row()) {
    for($i=0; $i<$fieldCount; $i++) {
        $export .= '"'.$row[$i].'",';
    }
    $export .="\r\n";
}

header("Content-type: text/x-csv");
header("Content-Disposition: attachment; filename=".$csv."");
header("Pragma: no-cache");
header("Expires: 0");
echo($export);
?>