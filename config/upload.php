<script src="../javascripts/jquery-3.1.1.min.js"></script>
<script src="../javascripts/upload.js"></script>
<?php 
require_once 'connection.php';
require_once 'variables.php';
require_once 'session.php';

$extensions = array('csv');
$batchSize = 1000;
$FILE = $_FILES['importer'];

$staticColumns = array("cnp", "departament", "prescurtare", "domeniu", "camera", "interior", "fax", "email");
$dynamicColumns = array("cnp", "titlu", "nume", "prenume");

if($FILE['error'] === 0 && isset($_POST['importType'])) {
    $importType = $_POST['importType'];
    switch($importType) {
        case "dynamic": $allowedColumns = $dynamicColumns;
                        $table = "contacts_dynamic";
                        break;
        case "static": $allowedColumns = $staticColumns;
                        $table = "contacts_static";
                        break;
    }
   
    $name = $FILE['name'];
    $tmpName = $FILE['tmp_name'];
    $rowCount = count(file($tmpName)) - 1;
    $extension = pathinfo($name, PATHINFO_EXTENSION);
    $columns = array();
    $tableColumns = "";
    $fileName = "";

    if(in_array($extension, $extensions)) {
        if(($handle = fopen($tmpName, 'r')) !== FALSE) {
            set_time_limit(0);
            $row = 1;
            if(($heading = fgetcsv($handle)) !== FALSE) {
                for($i = 0; $i < count($heading); $i++) {
                    if(in_array($heading[$i], $allowedColumns)) {
                        $columns[$heading[$i]] = $i;
                        $tableColumns .= $heading[$i] . ",";
                    }
                }
                $tableColumns = substr_replace($tableColumns ,"",-1);   
                $sql = "TRUNCATE TABLE $table";
                if(($result = $mysqli->query($sql)) !== TRUE) {
                    $_SESSION['upload_error'] = ERROR;
                }
            }

            while(($data = fgetcsv($handle)) !== FALSE) {    
                if($row % $batchSize === 1) {
                    $fileName = "import$row.csv";
                    $file = fopen($fileName, "w");
                }
                $json = "";
                foreach($columns as $colName=>$colIndex) {
                    if(strpos($data[$colIndex], "'") !== FALSE) {
                        $json .= "'".str_replace("'", "`", $data[$colIndex])."',";
                    } else {
                        $json .= "'".$data[$colIndex]."',"; 
                    }
                }
                $json = substr_replace($json, "", -1);
                fwrite($file, $json.PHP_EOL);
                
                if(($row % $batchSize === 0) || ($rowCount === 0)){
                    echo "<script>sendData('$fileName', '$tableColumns', '$table')</script>";
                }
                $row++;
                $rowCount--;
            }
            fclose($file);
            fclose($handle);
        }
    } else {
        $_SESSION["upload_error"] = EXTENSION_MISMATCH;
    }
}

?> 


