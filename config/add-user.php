<?php 
require_once 'connection.php';
require_once 'variables.php';
require_once 'session.php';

$email = $name = $password ="";
$register_errors = array();

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty(trim($_POST["email"]))) {
        array_push($register_errors, EMAIL_EMPTY);        
    } else {
        $sql = "SELECT user_id FROM users WHERE user_email = ?";
        
        if($stmt = $mysqli->prepare($sql)) {
            $stmt->bind_param("s", $param_user_email);
            $param_user_email = trim($_POST["email"]);

            if($stmt->execute()) {
                $stmt->store_result();

                if($stmt->num_rows == 1) {
                    array_push($register_errors, EMAIL_TAKEN);
                } else {
                    $email = trim($_POST["email"]);
                }
            } else {
                array_push($register_errors, FAIL);
            }
        }
        $stmt->close();
    }
    
    if(empty(trim($_POST["password"]))) {
        array_push($register_errors, PASSOWRD_EMPTY);
    } else {
        $password = trim($_POST['password']);
    }

    if(empty(trim($_POST["name"]))) {
        array_push($register_errors, NAME_EMPTY);
    } else {
        $name = trim($_POST['name']);
    }

    if(empty($register_errors)) {
        $sql = "INSERT INTO users (user_name, user_email, user_password) VALUES(?, ?, ?)";

        if($stmt = $mysqli->prepare($sql)) {
            $stmt->bind_param("sss", $param_name, $param_email, $param_password);
            $param_name = $name;
            $param_email = $email;
            $param_password = password_hash($password, PASSWORD_DEFAULT);
        
            if($stmt->execute()) {
                header("location: ../admin.php?page='users'");
            } else {
                array_push($register_errors, ERROR);
            }
        }
        $stmt->close();
    } else {
        $_SESSION["register_errors"] = $register_errors;
        header("location: ../admin.php?page='users'");

    }
}
?>