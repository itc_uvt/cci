<?php
require_once 'connection.php';
require_once 'variables.php';
require_once 'session.php';

$password_errors = array();
$newPassword = $confirmNewPassword = "";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $userId = $_POST["userId"];
    if(empty(trim($_POST["newPassword"]))) {
        array_push($password_errors, PASSWORD_EMPTY);
    } else {
        $newPassword = trim($_POST["newPassword"]);
    }

    if(empty(trim($_POST["confirmNewPassword"]))) {
        array_push($password_errors, PASSWORD_CONFIRM);
    } else {
        $confirmNewPassword = trim($_POST["confirmNewPassword"]);
    }

    if(!($newPassword === $confirmNewPassword)) {
        array_push($password_errors, PASSWORD_MISMATCH);
    }

    if(empty($passowrd_errors)) {
        $password = password_hash($newPassword, PASSWORD_DEFAULT);
        $sql = "UPDATE users SET user_password='$password' WHERE user_id='$userId';";
            
        if(!($result = $mysqli->query($sql))) {
            array_push($passowrd_errors, ERROR);
        }        
    }
    if(!empty($password_errors)) {
        $_SESSION["password_errors"] = $password_errors;
        header("location: ../admin.php?page='password'&user='$userId'");
    } else {
        header("location: ../admin.php?page='users'");
    }
}
?>