<?php 
require_once 'connection.php';
require_once 'variables.php';
require_once 'session.php';

$remove_errors = array();

if(isset($_GET["user"])) {
    $userId = $mysqli->real_escape_string(preg_replace("/\W+/", "", $_GET['user']));
    $sql = "SELECT user_email FROM users WHERE user_id = '$userId';";
    
    if($result = $mysqli->query($sql)) {
        if($val = $result->fetch_object()) {
            if($_SESSION['email'] === $val->user_email){
                array_push($remove_errors, USER_MYSELF);
            } else {
                $sql = "DELETE FROM users WHERE user_id = '$userId';";
                if(($mysqli->query($sql)) !== TRUE) {
                    array_push($remove_errors, ERROR);
                }
            }
        }
    }
}
$_SESSION["remove_errors"] = $remove_errors;
header("location: ../admin.php?page='users'");
?>