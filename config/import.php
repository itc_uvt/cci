<?php
require_once 'connection.php';
require_once 'variables.php';
require_once 'session.php';

$data = $_POST['file'];
$columns = $_POST['columns'];
$table = $_POST['table'];
$handle = fopen($data, "r");
$test = file_get_contents($data);

if ($handle) {
    $counter = 0;    
    $sql ="INSERT INTO $table($columns) VALUES ";
    while (($line = fgets($handle)) !== false) {
      $sql .= "($line),";
      $counter++;
    }
    $sql = substr($sql, 0, strlen($sql) - 1);
    if ($mysqli->query($sql) !== TRUE) {
        $_SESSION["upload_error"] = ERROR;
    }
    fclose($handle);
}
unlink($data);
?>