<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand" href="#"><?php echo $title; ?></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#">
                        <i class="fa fa-user text-primary"></i>
                        <p class=" text-primary"><?php echo htmlspecialchars($_SESSION['email']); ?></p>
                    </a>
                </li>
                <li>
                    <a href="config/logout.php">
                        <i class="fa fa-sign-out text-danger"></i>
                        <p class="text-danger">Delogare</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>