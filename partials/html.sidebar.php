<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="admin.php">
                <img src="images/logo_uvt_centenar.png" alt="Logo UVT" class="image-responsive logo-image"/>
            </a>
        </div>
        <ul class="nav">
            <li>
                <a href="admin.php">
                    <i class="fa fa-search"></i>
                    <p>Vizualizare</p>
                </a>
            </li>
            <li>
                <a href="admin.php?page='upload'">
                    <i class="fa fa-plus"></i>
                    <p>Incarcare</p>
                </a>
            </li>
            <li>
                <a href="admin.php?page='users'">
                    <i class="fa fa-users"></i>
                    <p>Utilizatori</p>
                </a>
            </li>
            <li>
                <a href="index.php">
                    <i class="fa fa-arrow-left"></i>
                    <p>Inapoi</p>
                </a>
            </li>
        </ul>
    </div>
</div>