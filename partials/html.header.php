<?php
echo '
<!DOCTYPE html> 
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>'. $title . ' | CCI UVT</title>
    <link href="stylesheets/bootstrap.min.css" rel="stylesheet">
    <link href="stylesheets/font-awesome.min.css" rel="stylesheet">
    <link href="stylesheets/fileinput.min.css" rel="stylesheet">
    <link href="stylesheets/dashboard.css" rel="stylesheet">
</head>
<body>';
?>