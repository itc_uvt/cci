<!DOCTYPE html>
<html lang="en">
<head>    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Contacte Interne | UVT</title>
    
    <link href="stylesheets/style.css" rel="stylesheet">
    <link href="stylesheets/bootstrap.min.css" rel="stylesheet">
    <link href="stylesheets/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div class="col-xs-10 col-sm-8 col-md-6 col-xs-offset-1 col-sm-offset-2 col-md-offset-3">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <a href="https://uvt.ro/">
                    <img src="images/logo_uvt_centenar.png" alt="Logo UVT" class="center-block logo">
                </a>         
            </div>
        </div>
        <hr />  
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <i class="text-justify">
                    <p>Acest serviciu va permite cautarea persoanelor angajate in UVT si aflarea unor informatii pentru contactarea lor: telefon, e-mail, cabinet/locatie. <a class="btn btn-link text-right" data-toggle="collapse" href="#infoText" role="button" aria-expanded="false" aria-controls="infoText">Mai multe detalii</a></p>
                    <div class="collapse" id="infoText">
                        <p>Telefoanele interioare din Universitatea de Vest din Timisoara pot fi apelate din exteriorul universitatiii, dupa urmatoarea regula: 592XXX, unde XXX = numarul interior din universitate (de exemplu: 592169 = Secretar Sef Universitate). </p>
                        <p>Pentru a cauta in baza de date, introduceti numele, prenumele, un numar de telefon interior (ex.100) sau numarul cabinetului (ex. 150b)!</p>
                        <p>Telefon centrala - informatii: <strong>0256/592.111</strong></p>
                    </div>
                </i>
            </div>                
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control input-lg" name="searchBox" id="searchBox" onkeyup="search(event)" autofocus>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <ul id="results" class="list-group"></ul>
            </div>
        </div>
    </div>
</body>
<script src='javascripts/jquery-3.1.1.min.js' type='text/javascript'></script>
<script src='javascripts/bootstrap.min.js' type='text/javascript'></script>
<script src='javascripts/search.js' type='text/javascript'></script>
</html>